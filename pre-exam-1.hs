-- https://wiki.haskell.org/99_questions/46_to_50
import Data.List

data TruthExpr = TruthAnd TruthExpr TruthExpr | TruthOr TruthExpr TruthExpr | TruthNot TruthExpr | TruthVal Bool | TruthVar Char deriving (Show)

evalTruth :: TruthExpr -> Bool
evalTruth (TruthAnd a b) = (evalTruth a) && (evalTruth b)
evalTruth (TruthOr a b) = (evalTruth a) || (evalTruth b)
evalTruth (TruthNot a) = not $ evalTruth a
evalTruth (TruthVal a) = a
evalTruth (TruthVar a) = error $ "Unbound variable '" ++ [a] ++ "'"

-- Returns a new TruthExpr with value subititued for variable
substitute :: TruthExpr -> Char -> Bool -> TruthExpr
substitute (TruthAnd a b) x v = TruthAnd (substitute a x v) (substitute b x v)
substitute (TruthOr a b) x v = TruthOr (substitute a x v) (substitute b x v)
substitute (TruthNot a) x v = TruthNot $ substitute a x v
substitute (TruthVal a) x v = TruthVal a
substitute (TruthVar a) x v = if a == x then TruthVal v else TruthVar a

--- Given a TruthExpr, return a truth table as a list of "truth rows" containing: value of each parameter, and value of TruthExpr
--truthTable :: TruthExpr -> [Char] -> [[Bool]]
-- TODO: Implement

-- Need to use list functor (or monad?) somehow....

data Tree a = Branch a (Tree a) (Tree a) | Empty deriving (Show, Eq)

countNodes :: Tree a -> Int
countNodes Empty = 0
countNodes (Branch v b c) = 1 + max (countNodes b) (countNodes c)

-- Is tree compleletely balanced? (# of nodes in left and right sub-trees are equal.)
isCbalTree :: Tree a -> Bool
isCbalTree Empty = True
isCbalTree (Branch v b c) = (countNodes b == countNodes c) && isCbalTree b && isCbalTree c

mirrorTree :: Tree a -> Tree a
mirrorTree Empty = Empty
mirrorTree (Branch v b c) = Branch v c b

isSymmetricTree :: (Eq a) => Tree a -> Bool
isSymmetricTree Empty = True
isSymmetricTree (Branch v b c) = (mirrorTree c) == b

treeHeight :: Tree a -> Int
treeHeight Empty = 0
treeHeight (Branch v b c) = 1 + (treeHeight b) + (treeHeight c)

isHeightBalancedTree :: Tree a -> Bool
isHeightBalancedTree Empty = True
isHeightBalancedTree (Branch v b c) = treeHeight b == treeHeight c


allTreesOfDepth :: (Eq a) => Int -> a -> [Tree a]
allTreesOfDepth 0 _ = [ Empty ]
allTreesOfDepth n v = nub $ [ Branch v a b | a <- allTreesOfDepth (n-1) v,
                                       b <- allTreesOfDepth (n-1) v ] ++ allTreesOfDepth (n - 1) v

symCbalTrees :: Int -> [Tree Char]
symCbalTrees n = [ t | t <- allTreesOfDepth n 'x', isSymmetricTree t, isCbalTree t ]

-- List of all height-balanced trees of depth n
hbalTrees :: (Eq a) => Int -> a -> [Tree a]
hbalTrees n v = [ t | t <- allTreesOfDepth n v, isHeightBalancedTree t ]

-- Missing: 57, 60
