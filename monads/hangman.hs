module Hangman (main) where

import Prob
import Control.Monad.Loops
import System.Environment
import System.IO
import System.Exit


type Guesses = [Char]
type GameWord = [Char]
data HangGame = Running Guesses GameWord
    deriving (Show, Eq, Ord)

type Env = [GameWord]

fileWords :: String -> IO [GameWord]
fileWords file = do
    h <- openFile file ReadMode
    contents <- hGetContents h
    return $ lines contents


randomGame :: Env -> ProbDist HangGame
randomGame env = fmap mkGame (uniformOnSample env)
    where mkGame word = Running [] word


gameGuess :: HangGame -> Char -> HangGame
gameGuess (Running gs word) x = Running (x : gs) word

isOver :: HangGame -> Bool
isOver (Running gs word) = length gs >= 5 || won (Running gs word)

won :: HangGame -> Bool
won (Running gs word) = showingLetters (Running gs word) == word

lost :: HangGame -> Bool
lost game = isOver game && not (won game)

showingLetters :: HangGame -> String
showingLetters (Running gs word) = word >>= (\x -> if x `elem` gs then [x, ' '] else "_ ")

gamePos :: HangGame -> String
gamePos (Running gs word) = showingLetters (Running gs word) ++ "(" ++ show (length word) ++ ")\n" 
    ++ "Guesses (" ++ (show $ length gs) ++ "/5): " ++ gs

playRound :: HangGame -> IO HangGame
playRound game = do
    putStrLn "Current position: " 
    putStrLn $ gamePos game ++ "\n"

    putStrLn "Enter your guess: "
    guess <- getLine 
    if (length guess) /= 1
        then do putStrLn "Not a legal guess"
                playRound game
        else return $ gameGuess game (guess !! 0)

playGame :: Env -> IO HangGame
playGame env = do
    game <- randomEvent $ randomGame env
    over <- iterateUntilM isOver playRound game
    return over

endOfGameMsg :: HangGame -> IO ()
endOfGameMsg game = do
    putStrLn $ "You " ++ (if won game then "won" else "lost") ++ " the game!"
    putStrLn $ gamePos game
    putStrLn ""
    putStrLn $ "Word was: " ++ (case game of (Running _ word) -> word)
    

main = do
    args <- getArgs
    if length args < 1 then do 
        putStrLn "Missing word file argument"
        exitFailure
    else do
        let wordFile = args !! 0
        env <- fileWords wordFile
        game <- playGame env

        endOfGameMsg game
