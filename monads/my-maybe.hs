import Control.Monad
import Control.Monad.Writer

data Weird = A | B

data MyAll = MyAll Bool
    deriving (Show, Eq)

instance Monoid MyAll where
    mempty = MyAll True
    mappend (MyAll x) (MyAll y) = MyAll (x && y)

data MySum = MySum Int deriving (Show, Eq)

instance Monoid MySum where
    mempty = MySum 0
    mappend (MySum x) (MySum y) = MySum $ x + y

join' :: Maybe (Maybe a) -> Maybe a
join' (Just x) = x
join' Nothing = Nothing

-- >>=' :: Maybe a -> (a -> Maybe b) -> Maybe b
--v >>=' f = join' ((fmap f) x)

data MyMaybe a = MyJust a | MyNothing
    deriving (Eq, Ord, Show, Read)

instance Functor MyMaybe where
    fmap f (MyJust x) = MyJust (f x)
    fmap f MyNothing = MyNothing

instance Applicative MyMaybe where
    pure = MyJust 
    (<*>) (MyJust f) = fmap f

instance Monad MyMaybe where 
    MyJust x >>= f = f x
    MyNothing >>= f = MyNothing
    
    _ >> x = x
    return x = MyJust x
    fail str = MyNothing

data MyTree a = MyEmpty | MyNode a [MyTree a]
    deriving (Eq, Ord, Show, Read)

instance Functor MyTree where
    fmap f MyEmpty = MyEmpty
    -- f :: a -> b; children :: [MyTree a]
    fmap f (MyNode x children) = MyNode (f x) ((fmap . fmap $ f) children)

--instance Applicative MyTree where
--    pure v = MyTree v []
--    (<*>) (MyTree f fTrees) MyEmpty = 
--    (<*>) (MyTree f fTrees) (MyTree x xTrees) = 
--    (<*>) (MyTree f fTrees) (MyTree x xTrees) = 

-- b is a monoid; a is any type
data MyWriter b a = MyWriter b a
    deriving (Show, Read, Eq, Ord)

-- Projection (losing structure)
myRunWriter :: (Monoid b) => MyWriter b a -> (a, b)
myRunWriter (MyWriter b a) = (a, b)

instance (Monoid b) => Functor (MyWriter b) where
    fmap f (MyWriter b a) = MyWriter b $ f a

-- Does this satisfy the re-ordering law?
instance (Monoid b) => Applicative (MyWriter b) where
    pure = MyWriter mempty
    (<*>) (MyWriter b1 f) (MyWriter b2 v) = MyWriter (b1 `mappend` b2) (f v)

instance (Monoid b) => Monad (MyWriter b) where
    return = pure
    (>>=) (MyWriter x a) f = let (resA, resB) = myRunWriter $ f a in MyWriter (x `mappend` resB) resA

type Log = [String]

addWithLog :: Int -> Int -> MyWriter Log Int
addWithLog a b = MyWriter (["Adding: " ++ show a ++ " + " ++ show b]) (a + b)

addWithResultBig :: Int -> Int -> MyWriter MyAll Int
addWithResultBig x y = MyWriter (MyAll $ x > 100) (x + y)

factorial :: Int -> MyWriter Log Int
factorial n = if n == 0 then MyWriter ["N == 0; we're done!"] 1 
                        else (factorial $ n - 1) >>= (\res -> MyWriter [show n ++ " * factorial (" ++ show (n - 1) ++ ")"] (n * res) )

factorial' :: Int -> MyWriter Log Int
factorial' n = if n == 0 then MyWriter ["N == 0; we're done!"] 1 
                        else do
                            res <- factorial' $ n - 1
                            MyWriter [show n ++ " * factorial (" ++ show (n - 1) ++ ")"] (res * n)

--factorial'' :: Int -> Writer Log Int
factorial'' 0 = writer (1, ["0! = 0; we're done!"]) 
factorial'' n = do
    res <- factorial'' $ n - 1
    writer (res * n, [show n ++ " * " ++ show (n - 1) ++ "!"] )
