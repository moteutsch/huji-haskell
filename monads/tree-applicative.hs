data MyTree a = MyEmpty | MyNode a [MyTree a]
    deriving (Eq, Ord, Show, Read)

instance Functor MyTree where
    fmap f MyEmpty = MyEmpty
    -- f :: a -> b; children :: [MyTree a]
    fmap f (MyNode x children) = MyNode (f x) ((fmap . fmap $ f) children)

appendToLeaves :: MyTree a -> [MyTree a] -> MyTree a
appendToLeaves MyEmpty _ = MyEmpty
appendToLeaves (MyNode v []) newChildren = MyNode v newChildren
appendToLeaves (MyNode v xs) newChildren = MyNode v (fmap ((flip appendToLeaves) newChildren) xs)

flatten :: MyTree (MyTree a) -> MyTree a
flatten MyEmpty = MyEmpty
flatten (MyNode MyEmpty _) = MyEmpty -- TODO: This seems weird. Maybe this motivates the dual construction?
flatten (MyNode tree childrenTrees) = tree `appendToLeaves` (fmap flatten childrenTrees)

instance Applicative MyTree where
    pure v = MyNode v []
    MyEmpty <*> _ = MyEmpty
    _ <*> MyEmpty = MyEmpty
    (MyNode f fNodes) <*> tree = (fmap f tree) `appendToLeaves` (fNodes <*> tree)


