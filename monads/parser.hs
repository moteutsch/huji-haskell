import Control.Applicative
import Data.Char

-- A parser consists of a sequence of inputs, and parsed output, corresponding to
-- steps in parsing.
data Parser a = Parser { runParser :: String -> [(a, String)] }

instance Functor Parser where
    -- Apply function to the result of each parsing step
    fmap f (Parser q) = Parser $ \s -> [(f y, ys) | (y, ys) <- q s]

instance Applicative Parser where
    pure v = Parser $ \s -> [(v, s)]
    Parser p <*> Parser q = Parser $ \s -> [(x y, ys) | (x, xs) <- p s, (y, ys) <- q xs]

-- We extend our parser (which is applicative) to be a monoid also
instance Alternative Parser where
    -- empty :: f a
    -- The identity. This is vaccuous for parsing, but will technically extend
    -- this from a semi-group (without identity) to a monoid, since extending
    -- any semi-group with some atom "e" s.t. for all x, e*x = e
    empty = Parser $ \s -> []
    Parser p <|> Parser q = Parser $ \s -> p s ++ q s

letter = Parser p where
    p (x:xs) | isAlpha x = [(x, xs)]
    p _ = []
