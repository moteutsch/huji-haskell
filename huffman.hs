import qualified Data.Tree as T

-- Binary tree definition
data BinaryTree a = Node a (BinaryTree a) (BinaryTree a) | Leaf a
    deriving (Eq, Show, Ord)

instance Functor BinaryTree where
    fmap f (Leaf a) = Leaf (f a)
    fmap f (Node a t1 t2) = Node (f a) (fmap f t1) (fmap f t2)

-- Projection of node value
nodeVal :: BinaryTree a -> a
nodeVal (Leaf x) = x
nodeVal (Node x t1 t2) = x

-- Natural transformation from BinrayTree to Data.Tree.Tree
toDataTree (Leaf x) = T.Node x []
toDataTree (Node x t1 t2) = T.Node x [toDataTree t1, toDataTree t2]

-- Sorting algorithm
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:[]) = [x]
quicksort (mid:xs) = (quicksort left) ++ middle ++ (quicksort right)
    where left = [ x | x <- xs, x < mid]
          middle = [ x | x <- xs, x == mid ] ++ [mid]
          right = [ x | x <- xs, x > mid]

-- Huffman encoding of symbols. Encoding of character is given by path to 
-- character in binary tree. O(nlogn)
huffman :: [(Char, Float)] -> BinaryTree (Char, Float)
huffman = listToBinaryTree . quicksort 

-- Input is already sorted
listToBinaryTree :: [(Char, Float)] -> BinaryTree (Char, Float)
listToBinaryTree = joinNodes . (fmap Leaf)

-- Input is already sorted
joinNodes :: [BinaryTree (Char, Float)] -> BinaryTree (Char, Float)
joinNodes = foldl1 (\t1 t2 -> Node ('+', (snd $ nodeVal t1) + (snd $ nodeVal t2)) t1 t2)

-- Display a Huffman code by natural transformation to Data.Tree and 
-- using their "drawTree" method
displayHuffman :: BinaryTree (Char, Float) -> IO ()
displayHuffman t = do
    let ct = fmap (\(x, val) -> "The value " ++ [x] ++ " has weight " ++ show val) t
    putStrLn $ T.drawTree $ toDataTree ct
