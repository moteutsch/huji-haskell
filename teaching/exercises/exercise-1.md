# Lifts (aka "map" aka "fmap")

Suppose we have some function \( g : a -> b \), and a list of \( a \)s. How do we construct a list of \( b \)s? For example:

    f x = x * x
    list_as = [1, 2, 3, 4, 5]
    list_bs = [1, 4, 9, 16, 25]

    [1, 2, 3, 4, 5] == [f 1, f 2, f 3, f 4, f5]  -- True

How do we construct \( list_bs \) in general, for any \( f \) and any two types? A function that does this is called a "lift" of \( f \). It must be of type \( (a -> b) -> [a] -> [b] \). I.e., given a function \( f \) form \( a -> b \) and a list of \( a \)s, it returns a list of \( b \)s. 

Let's define such a function and call it "map'":

    map' :: (a -> b) -> [a] -> [b]

How could we define such a function? In an imperative language we would probably write a loop over the elements of the list. This should hint to us that our function is probably definable by *structural recursion* on lists. What is the base case of a recursive/inductive argument on a list? The empty list.

    map f [] = []

So, it our list is empty we return the same list. What if it is non-empty? We will define it based on a smaller part of itself. Recall that lists are a recursive structure: a list is either empty or it is the concatenation of an element to an existing list. We can pattern match on the structure of the list to define it:

    map f (x:xs) = error "We don't know what to do here yet"

We've written a definition of "map" for the case that the second argument is of the form "x" and then another list "xs". We can now use the induction hypothesis: we assume that we've already defined \( map \) on smaller lists than the one we're considering. So we can assume that "map g xs" is defined for any function \( g \). (We can also think of this as a definition by recursion on the *length* of our list. We've defined it for lists of length zero, and now we're defining for lists of length \( n + 1 \), assuming it is already defined for lists of length \( n \).)

    map f (x:xs) = (f x) : (map f xs)

Let's consider a run of \( map f ls \) for \( f = \x -> x * x \) and \( ls = [1, 2, 3, 4, 5] \):

    map f [1, 2, 3, 4, 5] = (f 1) : (map f [2, 3, 4, 5])
    = 1 : ((f 2) : map f [3, 4, 5]) = 1 : (4 : (f 3) : (map f [4, 5]))
    = 1 : (4 : (9 : (f 4) : (map f [5])))
    = 1 : (4 : (9 : (16 : (f 5) : (map f []))))
    = 1 : (4 : (9 : (16 : (25 : (map f [])))))
    = 1 : (4 : (9 : (16 : (25 : []))))
    = [1, 4, 9, 16, 25]

The run of \( map \) is almost as if it were implemented by a loop that "accumulates" a result and returns it: it first applies to the function to the first element, then to the second, and so on. The final result is the list \( [f 1, f 2, f 3, f 4, f 5] \).

### Monoids (or semi-groups)

Definition: A monoid (A, e, +) is a triple: A is some set, \( e \in A \), \( + \) is an associative binary operator on elements of A. I.e., \( + : A \times A \to A \), and e is the identity with respect to \( A \). I.e., for all \( a \in A \), \( e + a = a + e = a \).

Examples:

1. The set of strings (i.e., the type \( String \) ) forms a monoid: the empty string is the identity, the binary operator is string concatenation ( \( ++ \) ). Indeed, if \( a : String \),

    "" ++ a == a ++ "" = a

And associativity is obvious:

    "Hello " ++ ("world" ++ "!") == ("Hello " ++ "world") ++ "!"

2. More generally, if \( a \) is a type, the type \( [a] \) forms a monoid in the same way, with the identity being the empty list. (Recall that \( String = [Char] \), so that example 1 is a specific case of example 2.)

    [] ++ [1, 2, 3, 4] = [1, 2, 3, 4] ++ [] = [1, 2, 3, 4]

3. The type \( Int \) is a monoid in two different ways:
3.1. Take \( e = 0 \) to be the identity and the oeprator to be \( + : Int -> Int -> Int \) addition.
3.2. Take \( e = 1 \) and the operator to be: \( * : Int -> Int -> Int \) multiplication.


### Getting those monoids to work

If we have some monoid, an obvious thing we can do is "multiply" a list of elements. For example, if we have:

    lines = ["Hello\n", "Goodbye\n", "etc.\n"]

We might want to get the flattened string:

    all_lines = "Hello\nGoodbye\netc.\n"

For another example, we might have:

    earnings_per_week = [172, 48, 11, 42]

We might want to get the sum of them:

    all_time_earnings = 172 * 48 * 11 * 42

Let's define a couple of these:

    multiply_list :: [Int] -> Int
    multiply_list [] = 1
    multiply_list (x:xs) = x * (multiply_list xs)

Again we define it by recursion. 
