# Haskell Http Server

+ HTTP request parser
+ HTTP response generator
+ Router (like Express, but more minimal. See: https://expressjs.com/en/guide/routing.html)
    + Handle route-handlers
    + Handle files
+ Web server
    + Threads listening for connections (TCP)
    + On connection, read all and parse with HTTP parser
    + Match best route with router
    + Run "response generator" on result of route and send (TCP)

## Like-to-haves

+ Handling of methods other than "POST" and "GET"
+ Basic security features
+ Stream-based (ByteString) handling, not Haskell "String"s

## Bugs:

+ http://stackoverflow.com/questions/10485740/hgetcontents-being-too-lazy
