import Text.Parsec.Char (endOfLine)
import Text.ParserCombinators.Parsec hiding ((<|>), many, optional)
import Control.Applicative
import Control.Monad
import Data.Maybe (fromMaybe)

import Network (listenOn, withSocketsDo, accept, PortID(..), Socket)
import System.Environment (getArgs)
import System.IO (hFlush, hClose, hSetBuffering, hGetLine, hGetContents, hPutStr, hPutStrLn, BufferMode(..), Handle)
import Control.Concurrent (forkIO)


data Method = POST | GET
    deriving (Show, Eq)
type Path = String
type RequestLine = (Method, Path)
type Header = (String, String)
type Body = String
type HttpRequest = (Method, Path, [Header], Maybe Body)

tillEol = manyTill anyChar (try endOfLine)


httpRequest :: Parser HttpRequest
httpRequest = do
    requestLine <- initialLine
    headers <- many $ headerLine
    blankLine
    body <- optional $ messageBody
    return (fst $ requestLine, snd $ requestLine, headers, body)

initialLine :: Parser RequestLine
initialLine = do
    method <- (
            (string "GET" *> pure GET <|> string "POST" *> pure POST) 
            <* (many $ oneOf [' '])
        )
    url <- tillEol
    endOfLine
    return (method, url)

headerLine :: Parser Header
headerLine = do
    key <- many $ alphaNum <|> char '-'
    string ":"
    many $ oneOf [' ']
    value <- tillEol
    return (key, value)

blankLine :: Parser ()
blankLine = endOfLine *> pure ()

messageBody :: Parser String
messageBody = many $ anyChar



type ResponseCode = (Int, String)
type HttpResponse = (ResponseCode, [Header], Maybe Body)
type Handler = HttpRequest -> HttpResponse

ok :: String -> HttpResponse
ok body = ((200, "OK"), [], (Just $ body))

responseToString :: HttpResponse -> String
responseToString (code, headers, body) = "HTTP/1.0 " ++ show (fst code) ++ " " ++ show (snd code) ++ "\r\n"
  ++ (joinWith (fmap (\(key, value) -> key ++ ": " ++ value) headers) "\r\n") ++ "\r\n"
  ++ fromMaybe "" body




data Route = Route Method String Handler

-- Route helpers
route = Route
get = route GET
post = route POST


router :: [Route] -> RequestLine -> Route
router routes (reqMethod, reqPath) = head . (filter routeMatches) $ routes
    where routeMatches (Route method path handler) = (method == reqMethod) && (path == reqPath)

main :: IO ()
main = withSocketsDo $ do
  
    let routes = [get "/" (\r -> ok "<h1>Hello, world!</h1>")]

    args <- getArgs
    let port = fromIntegral (read $ head args :: Int)
    sock <- listenOn $ PortNumber port
    putStrLn $ "Listening on " ++ (head args)
    sockHandler routes sock


sockHandler :: [Route] -> Socket -> IO ()
sockHandler routes sock = do
    (handle, _, _) <- accept sock
    hSetBuffering handle NoBuffering
    forkIO $ commandProcessor routes handle
    sockHandler routes sock

commandProcessor :: [Route] -> Handle -> IO ()
commandProcessor routes handle = do
    contents <- hGetContents handle

    putStrLn contents
    putStrLn "Hello"

    -- line <- hGetLine handle
    let parsed = parse httpRequest "Http parser" contents

    --let cmd = words line
    case (parsed) of
        Right (m, p, hs, body) -> 
            case (router routes (m, p)) of 
                (Route m p handler) -> do 
                    putStrLn "Hello"
                    let res = responseToString $ handler (m, p, hs, body)
                    putStrLn res

                    hPutStr handle res
                    hFlush handle

        Left e -> do 
            putStrLn (show e)
            hPutStrLn handle (show e)

    hClose handle


-- Probably already exists...
joinWith :: [String] -> String -> String
joinWith [] sep = ""
joinWith (x:xs) sep = x ++ sep ++ (joinWith xs sep)
