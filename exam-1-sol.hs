-- Comments: 
-- 1. Made some typos in actual test when defining "myTree"
-- 2. On test I named "length" and "map" instead of "length'" and "map'".
-- 3. Could have defined "MinusTree" to have three constructors instead of two with "Maybe".
--    Would have been more elegant.
-- 
-- But after correcting typos (1 and 2) the code compiles no problem and gives the correct solution
-- when running "evalMinus myTree" (6)

length' :: [a] -> Int
length' = foldr (\x l -> l + 1) 0

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x xs -> (f x) : xs) []

data MinusTree = Minus MinusTree (Maybe MinusTree) | Leaf Int

myTree :: MinusTree
myTree = Minus ( Minus (Leaf 4) (Just (Minus (Leaf 5) Nothing))) (Just (Minus (Minus (Leaf 3) Nothing) Nothing))

evalMinus :: MinusTree -> Int
evalMinus (Leaf n) = n
evalMinus (Minus t1 Nothing) = -(evalMinus t1)
evalMinus (Minus t1 (Just t2)) = (evalMinus t1) - (evalMinus t2)
