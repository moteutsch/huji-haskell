data Nat = Zero | Succ Nat
    deriving (Show)

natToInt :: Nat -> Int
natToInt Zero = 0
natToInt (Succ n) = (natToInt n) + 1

intToNat :: Int -> Nat
intToNat 0 = Zero
intToNat n = Succ $ intToNat (n - 1)

