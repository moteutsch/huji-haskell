import Data.Foldable
import Data.Maybe
import Data.List
import qualified Data.Map as Map
import Control.Monad.Fix

-- Fix point functions

factorial :: Int -> Int
factorial = fix g
  where g x = \n -> if n == 0 then 1 else n * x(n - 1)

length' :: [a] -> Int
length' = fix g
  where g x = \ls -> if null ls then 0 else 1 + (x $ tail ls)

-- Haskell Wiki solutions

gcd' :: Int -> Int -> Int
gcd' m n 
  | n == 0    = m
  | m < n     = gcd' n m
  | otherwise = gcd' n (m `mod` n)

isPrime :: Int -> Bool
isPrime n 
  | n <= 0    = error "Undefined"
  | n == 1    = False
  | otherwise = all (==1) $ map (gcd' n) [1..upperSquareN] -- Map GCD with (1, k) until k > sqrt(n)
      where upperSquareN = min ((round . sqrt . fromIntegral $ n) + 2) (n - 1)

coprime :: Int -> Int -> Bool
coprime a b = 1 == gcd' a b

-- Number of numbers less than n which are coprime to it
totient :: Int -> Int
totient n = length . filter (coprime n) $ [1..n-1]

factor :: Int -> Int -> Bool
a `factor` b 
  | a > b     = False
  | otherwise = not (coprime a b)

primeFactors :: Int -> [Int]
primeFactors n
  | n < 1 = error "Invalid input"
  | n == 1 = []
  | otherwise = case firstFactor of Nothing -> [n]
                                    Just k -> [k] ++ primeFactors (n `div` k)
      where firstFactor = find (\a -> a `factor` n) [1..n-1]

primeFactorsMult :: Int -> [(Int, Int)]
primeFactorsMult = wordNums . primeFactors

-- totient improved
phi :: Int -> Int
phi = foldl' (*) 1 . map (\(p, m) -> ((p - 1) * (p `pow` (m - 1)))) . primeFactorsMult

-- Number of primes in closed interval [a,b]
primesR :: Int -> Int -> [Int]
primesR a b
  | a > b = error "Invalid interval"
  | otherwise = filter isPrime [a..b]

goldbach :: Int -> Maybe (Int, Int)
goldbach n = listToMaybe [ (a, b) | a <- nPrimes, b <- nPrimes, a + b == n ]
    where nPrimes = filter isPrime [2..n]

-- Find golbach pairs for even numbers in interval
goldbachList :: Int -> Int -> [Maybe (Int, Int)]
goldbachList a b
  | a > b = error "Invalid interval"
  | otherwise = map goldbach $ filter even [a..b]

--- (Equivalent) helper functions for finding "number of appearances summary" for elements of list

wordNums :: (Eq a, Ord a) => [a] -> [(a, Int)]
wordNums = map (\xs -> (head xs, length xs)) . group . sort

wordNums' :: (Eq a, Ord a) => [a] -> [(a, Int)]
wordNums' = Map.toList . Map.fromListWith (+) . map (\x -> (x, 1))
--
-- Helper functions. (Wrote this code without internet.)

pow :: Int -> Int -> Int
a `pow` b
  | b < 0 = error "Invalid power"
  | otherwise = foldl' (*) 1 $ replicate b a

