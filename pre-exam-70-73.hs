---- 70-73: https://wiki.haskell.org/99_questions/70B_to_73

-- Problem 70B
data MultTree a = Node a [MultTree a] deriving (Show, Eq)

-- Problem 70C
nnodes :: MultTree a -> Int
nnodes (Node v xs) = 1 + (sum $ map nnodes xs)

-- Problem 70
stringToTree :: String -> MultTree Char
stringToTree (x:'^':"") = Node x []
stringToTree (x:xs) = Node x ys
    where level0 = map fst . filter ((==0) . snd) . zip [0..] . scanl (+) 0 . map (\v -> if v == '^' then -1 else 1) $ xs
          ys = map (stringToTree . uncurry (sub xs)) $ zip (init level0) (tail level0)
          sub s a b = take (b - a) $ drop a s

treeToString :: MultTree Char -> String
treeToString (Node x []) = x : '^' : []
treeToString (Node x cs) = [x] ++ concatMap treeToString cs ++ "^"

-- Problem 71
ipl :: MultTree Char -> Int
ipl (Node _ []) = 0
ipl (Node _ xs) = sum $ map ((+1) . ipl) xs

-- Missing: 72, 73
