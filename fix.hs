g :: (Integer -> Integer) -> (Integer -> Integer)
g x = \n -> if n== 0 then 1 else n * x (n - 1)

x0 :: Integer -> Integer
x0 = undefined

(f0:f1:f2:f3:f4:fs) = iterate g x0
