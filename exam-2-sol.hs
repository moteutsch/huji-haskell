data L a = N | C a (L a) 
    deriving (Show)

instance Functor L where
    fmap f N = N
    fmap f (C x xs) = C (f x) (fmap f xs)

addToEnd :: a -> L a -> L a
addToEnd x N = C x N
addToEnd x (C y ys) = C y (addToEnd x ys)

myConcat :: L a -> L a -> L a
myConcat N xs = xs
myConcat xs N = xs
myConcat xs (C y ys) = myConcat (y `addToEnd` xs) ys

flatten :: L (L a) -> L a
flatten N = N
flatten (C xs xss) = xs `myConcat` (flatten xss)

instance Applicative L where
    pure x = C x N
    N <*> _ = N
    _ <*> N = N
    (C f fs) <*> xs = (fmap f xs) `myConcat` (fs <*> xs)

instance Monad L where
    return = pure
    xs >>= f = flatten . (fmap f) $ xs

--- Example

liker y x = x ++ " likes " ++ y
example1 = (C (liker "Choclate") (C (liker "Haskell") (C (liker "Quizes") N))) <*> (C "Moshe" (C "Tom" (C "Jerry" N)))
